Mine = {}
function Mine:init (posx, posy, bounciness, friction)
  self.image = love.graphics.newImage ("Sprites/mine.png")
  self.body = love.physics.newBody (world, posx, posy, 'dynamic')
  self.shape = love.physics.newCircleShape (14)
  self.fixture = love.physics.newFixture (self.body, self.shape, 1)
  self.fixture:setRestitution (bounciness)
  self.fixture:setFriction (friction)
end

function Mine:draw ()
  love.graphics.draw (self.image, self.body:getX(), self.body:getY(), self.body:getAngle (), 1, 1, 14, 14)
end