require "scenery"
require "pworld"
require "pobject"

w_width = 900
w_height = 600

function love.load ()
  love.window.setMode (w_width, w_height, {resizable = false})
  love.graphics.setBackgroundColor (0.5, 0.8, 1, 1)
 
  terrainG = Scenery
  terrainG:init ()
  
  terrainP = Earth
  terrainP:init (terrainG)
  
  mine = Mine
  mine:init (470, 28, 0.8, 1)  
end
  
function love.update (dt)
  world:update(dt)
end 

function love.draw ()
  love.graphics.setColor(1, 1, 1, 1);
  mine:draw ()

  terrainG:draw ()
end
