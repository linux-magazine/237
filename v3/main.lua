require "scenery"
require "pworld"
require "pobject"
require "collide"

w_width = 900
w_height = 600

function love.load ()
  love.window.setMode (w_width, w_height, {resizable = false})
  love.graphics.setBackgroundColor (0.5, 0.8, 1, 1)
  
  world:setCallbacks(beginContact, endContact, preSolve, postSolve)
 
  terrainG = Scenery
  terrainG:init ()
  
  terrainP = Earth
  terrainP:init (terrainG)
  
  mine = Mine
  mine:init (150, terrainG.level01, 0.8, 1)
  aiming = true
  fire = false
  angle = 0
  force = 0
  
  tank = Tank
  tank:init (math.random (616, 884), terrainG.level02)
  tank_hit = false
  
end
  
function love.update (dt)
  world:update(dt)
    
  if aiming then
    if love.keyboard.isDown("right") and mine.body:getX () < 286 then
      mine.body:setX (mine.body:getX () + 1)
    elseif love.keyboard.isDown("left") and mine.body:getX () > 15 then
      mine.body:setX (mine.body:getX () - 1)
    elseif love.keyboard.isDown("up") and angle < 90 then
      angle = angle + 1
    elseif love.keyboard.isDown("down") and angle > 0 then
      angle = angle - 1
    elseif love.keyboard.isDown ("+") and force < 1000 then
      force = force + 1
    elseif love.keyboard.isDown("-") and force > 0 then
      force = force - 1    
    elseif love.keyboard.isDown("space") then
      aiming = false
      fire = true
    end     
  end
  
  if fire then
    mine.body:applyLinearImpulse (math.cos (math.rad (angle)) * force, math.sin (math.rad (angle)) * force)
    fire = false    
  end  
  
  if tank_hit then
    love.event.push('quit')
  end  
end 

function love.draw ()
  love.graphics.setColor(1, 1, 1, 1);
  love.graphics.print ('Angle: ' .. angle , 10, 10, 0, 2)
  love.graphics.print ('Force: ' .. force , 10, 40, 0, 2)
  
  mine:draw ()
  tank:draw ()
  
  terrainG:draw ()
end
