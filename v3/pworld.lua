love.physics.setMeter (30)
world = love.physics.newWorld (0, 9.81 * 30, true)

Earth = {}
function Earth:init (terrain)
  ground = {}
  for i=1, #terrain.groundT do
    ground[i] = {}
    ground[i].body = love.physics.newBody (world, 0, 0, 'static')
    ground[i].shape = love.physics.newPolygonShape (terrain.groundT [i])
    ground[i].fixture = love.physics.newFixture (ground [i].body, ground [i].shape)
  end
end