Mine = {}
function Mine:init (posx, posy, bounciness, friction)
  self.image = love.graphics.newImage ("Sprites/mine.png")
  self.body = love.physics.newBody (world, posx, posy, 'dynamic')
  self.shape = love.physics.newCircleShape (14)
  self.fixture = love.physics.newFixture (self.body, self.shape, 1)
  self.fixture:setRestitution (bounciness)
  self.fixture:setFriction (friction)
  self.fixture:setUserData ("Mine")
end

function Mine:draw ()
  love.graphics.draw (self.image, self.body:getX(), self.body:getY(), self.body:getAngle(), 1, 1, 14, 14)
end
---
Tank = {}
function Tank:init (posx, posy)
  self.image = love.graphics.newImage ("Sprites/tank.png")
  self.body = love.physics.newBody (world, posx, posy, 'dynamic')
  self.shape = love.physics.newRectangleShape (32, 19)
  self.fixture = love.physics.newFixture (self.body, self.shape, 1)
  self.fixture:setUserData ("Tank")
end

function Tank:draw ()
  love.graphics.draw (self.image, self.body:getX(), self.body:getY(), 0, 1, 1, 16, 10)
end