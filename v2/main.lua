require "scenery"
require "pworld"
require "pobject"
require "collide"

w_width = 900
w_height = 600

function love.load ()
  love.window.setMode (w_width, w_height, {resizable = false})
  love.graphics.setBackgroundColor (0.5, 0.8, 1, 1)
  
  world:setCallbacks(beginContact, endContact, preSolve, postSolve)
 
  terrainG = Scenery
  terrainG:init ()
  
  terrainP = Earth
  terrainP:init (terrainG)
  
  mine = Mine
  mine:init (470, 28, 0.8, 1)
  
  tank = Tank
  tank:init (math.random (616, 884), terrainG.level02)
  tank_hit = false  
end
  
function love.update (dt)
  world:update(dt)
  
  if tank_hit then
    love.event.push('quit')
  end  
end 

function love.draw ()
  love.graphics.setColor(1, 1, 1, 1);
  mine:draw ()
  tank:draw ()  

  terrainG:draw ()
end